# ------------------------------------------------------------ #
# Ubiqum Module 1, Sprint 2
# Customer Brand Preference Predictions
# Author: Danielle Jeffery
# Date: February 2020
# Version: 0.1
# ------------------------------------------------------------ #

# load libraries
library(carat)
library(ggplot2)

# read data
surveyData <- read.csv2("C:/Users/Danielle/Repositories/R_Projects/Ubiqum/Module_1/data/surveyBelkinElago.csv")

# split data
inTrain <- createDataPartition(
  y = surveyData$
)
